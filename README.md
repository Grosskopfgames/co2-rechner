# CO2 Rechner

In German, because it has only been fed data from german supermarkets, it will propably default everything to 15000g CO2-equivalents per kilo for other languages, if you want this for another area and know how to code Python, talk to me and maybe we can find a way.

Ein Python Script, das versucht für jede Zutatenliste einen CO2-Äquivalent-Wert pro Kilogramm Lebensmittel zu berechnen

Auf Deutsch, da es bisher nur mit Werten des deutschsprachigen Raums gefüttert wurde und daher nichts anderes unterstützt

Ich bin mir unsicher, ob man das schon als sehr dummes, sehr uneigenständiges neuronales Netz bezeichnen kann. Wahrscheinlich nicht, es ist eher eine mehrwurzelige Baumstruktur, die mit Userinput langsam wächst. Bitte helft mit, nutzt es, korrigiert meine tausende Rechtschreibfehler und schreibt mir, und wenn ihr große Datenbanken habt, schmeißt das Script einfach mal dagegen, vielleicht kommt ja was bei raus oder eins der Netze wächst.

Installation:

es braucht vermutlich folgende Pakete:

pip3 install dicttoxml &&
pip3 install parseString &&
pip3 install readchar

Nutzung:

python3 RelationsNet.py

zeigt die Anleitung ^^