#!/usr/bin/env python
# coding: utf8
import sqlite3
import os
import time
import json
import datetime
from lxml import html
#import requests
import threading
import sys
import readchar
from textwrap import wrap
import os
from dicttoxml import dicttoxml, escape_xml
from xml.dom.minidom import parseString
from datetime import datetime


debug=""

# CO2Emissions aus quelle WWF müssen raus, deren Quellen sind kacke, quelle Ökoinstitut hat ne neue tabelle
Items={}
Foodtype=["vegan","vegetarisch","unbekannt","fleisch","kuenstliches Kleinteil","allergen"]
# Netzwerkelement besteht aus:
# Wurzelelement wenns nicht selbst wurzel ist, sonst "",
# type(vegan=0, vegetarisch=1, unbekannt=2, fleisch=3 usw.)
# CO2emissions aus quelle CO2 Zaehler
# CO2emissions aus quelle WWF
# CO2emissions aus quelle Eigene schätzung 
# Unterschied wenn Bio (prozent),
# Unterschied wenn saisonal,
# Saisonanfang, Saisonende,
# UnterschiedTiefkuehl,
# UnterschiedKonserve,
ToDealWith={}
ToDealWithTexts={}
Network={"butter":			{"parent":"-","Foodtype":1,"CO2_Pendos":23800,	"CO2_Oeko":23600,	"CO2_WWF":14770,"BioDiffPercentage":-7},
"rindfleisch":				{"parent":"-","Foodtype":3,"CO2_Oeko":13350,	"CO2_Pendos":13300,	"CO2_WWF":20650,"BioDiffPercentage":-15,"DiffCoolingAbsolute":850},
"sonstiges fleisch":			{"parent":"-","Foodtype":3,"CO2_Oeko":4920,			"CO2_Wiegmann":5050,	"CO2_WWF":11940,"BioDiffPercentage":-15,"DiffCoolingAbsolute":850},
"kaese":				{"parent":"-","Foodtype":1,"CO2_Oeko":8230,	"CO2_Pendos":8500,	"CO2_Wiegmann":8350,	"CO2_WWF":7840, "BioDiffPercentage":-6},
"rohwurst":				{"parent":"-","Foodtype":3,"CO2_Oeko":7810,	"CO2_Pendos":8000,			"BioDiffPercentage":-7,"DiffCoolingAbsolute":800},
"koch+bruehwurst":			{"parent":"-","Foodtype":3,"CO2_Oeko":2450,	"BioDiffPercentage":-7,"DiffCoolingAbsolute":800},
"sahne":				{"parent":"-","Foodtype":1,"CO2_Oeko":7430,	"CO2_Pendos":7600,	"CO2_WWF":3280, "BioDiffPercentage":-7},
"pommes frites tiefgekuehlt":		{"parent":"-","Foodtype":1,"CO2_Pendos":5670,	"CO2_Pendos":5700,			"BioDiffPercentage":-3},
"schweineschinken":			{"parent":"-","Foodtype":3,"CO2_Oeko":4670,	"CO2_Pendos":4800,			"BioDiffPercentage":-6,"DiffCoolingAbsolute":800},
"kartoffelfertigprodukt (trocken)":	{"parent":"-","Foodtype":1,"CO2_Oeko":3720,	"CO2_Pendos":3750,			"BioDiffPercentage":-11},
"gefluegel":				{"parent":"-","Foodtype":3,"CO2_Pendos":3500,	"CO2_Oeko":443,	"CO2_WWF":4220, "BioDiffPercentage":-13,"DiffCoolingAbsolute":1000},
"schweinefleisch":			{"parent":"-","Foodtype":3,"CO2_Oeko":3350,	"CO2_Pendos":3250,	"CO2_WWF":7990, "BioDiffPercentage":-6,"DiffCoolingAbsolute":800},
"koch- und bruehwurst":			{"parent":"-","Foodtype":3,"CO2_Pendos":2500,			"BioDiffPercentage":-6,"DiffCoolingAbsolute":800},
"kondensmilch":				{"parent":"-","Foodtype":1,"CO2_Pendos":2500,	"CO2_WWF":3280},
"pflanzenoel":				{"parent":"-","Foodtype":0,"CO2_Pendos":2250,	"CO2_WWF":2480},
"oel-mix":				{"parent":"-","Foodtype":2,"CO2_Pendos":1890},
"eier":					{"parent":"-","Foodtype":1,"CO2_Pendos":1950,	"CO2_Oeko":1820,	"CO2_Wiegmann":1950,	"CO2_WWF":2000, "BioDiffPercentage":-21},
"quark und frischkaese":		{"parent":"-","Foodtype":1,"CO2_Oeko":1850,	"CO2_Pendos":1950,	"CO2_WWF":1760, "BioDiffPercentage":-8},
"obstsaft":				{"parent":"-","Foodtype":0,"CO2_Oeko":1610,	"CO2_Pendos":1650},
"obstsaftschorle":			{"parent":"-","Foodtype":0,"CO2_Pendos":805},
"limo":					{"parent":"-","Foodtype":0,"CO2_Pendos":510},
"zucker":				{"parent":"-","Foodtype":0,"CO2_Oeko":1460,	"CO2_Pendos":1500,	"CO2_WWF":2810, "BioDiffPercentage":-10},
"margarine":				{"parent":"-","Foodtype":0,"CO2_Pendos":1280,	"CO2_Pendos":1350,	"CO2_WWF":2480, "BioDiffPercentage":-22},
"pizza tiefgekuehlt":			{"parent":"-","Foodtype":2,"CO2_Oeko":1150,	"CO2_Pendos":1250,			"BioDiffPercentage":-24},
"joghurt":				{"parent":"-","Foodtype":1,"CO2_Oeko":1160,	"CO2_Pendos":1250,	"CO2_Wiegmann":1240,	"CO2_WWF":1760, "BioDiffPercentage":-8},
"konfituere":				{"parent":"-","Foodtype":1,"CO2_Oeko":1140,	"CO2_Pendos":1200},
"kakaobutter":				{"parent":"-","Foodtype":1,"CO2_Eigene_Schaetzung":2000,		"CO2_WWF":2490},
"milch":				{"parent":"-","Foodtype":1,"CO2_Pendos":889,	"CO2_Pendos":950,	"CO2_WWF":1760, "BioDiffPercentage":-5},
"feinbackwaren":			{"parent":"-","Foodtype":1,"CO2_Pendos":950,	"CO2_WWF":1680, "BioDiffPercentage":-11,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},
"teigwaren":				{"parent":"-","Foodtype":1,"CO2_Oeko":854,	"CO2_Pendos":900,	"CO2_Wiegmann":930,	"CO2_WWF":1680, "BioDiffPercentage":-17,"DiffCoolingAbsolute":-250,"DiffConservationAbsolute":350},
"mischbrot":				{"parent":"-","Foodtype":0,"CO2_Pendos":750,	"CO2_Oeko":728,	"CO2_Wiegmann":780,	"CO2_WWF":1680, "BioDiffPercentage":-13,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},
"broetchen und weissbrot":		{"parent":"-","Foodtype":0,"CO2_Pendos":650,	"CO2_Oeko":614,	"CO2_WWF":1680, "BioDiffPercentage":-15,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},
"weizenmehl":				{"parent":"-","Foodtype":0,"CO2_Pendos":600,	"CO2_WWF":1680, "BioDiffPercentage":-25},
"aepfel":				{"parent":"-","Foodtype":0,"CO2_Pendos":550,	"CO2_WWF":980,	"BioDiffPercentage":-10,"DiffCoolingAbsolute":0,"DiffConservationAbsolute":750},
"obst (mix)":				{"parent":"-","Foodtype":0,"CO2_Pendos":427,	"CO2_Pendos":450,	"CO2_Wiegmann":460,	"CO2_WWF":980,	"BioDiffPercentage":-10,"DiffCoolingAbsolute":0,"DiffConservationAbsolute":750},
"bier":					{"parent":"-","Foodtype":0,"CO2_Pendos":450,	"CO2_Oeko":416,	"CO2_WWF":1100, "BioDiffPercentage":-6},
"tomaten":				{"parent":"-","Foodtype":0,"CO2_Oeko":314,	"CO2_Pendos":350,	"CO2_WWF":900,	"BioDiffPercentage":-29,"DiffOutOfSeasonAbsolute":9100,"SeasonBegin":5,"SeasonEnd":10,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},
"erdbeeren":				{"parent":"-","Foodtype":0,"CO2_Pendos":300,	"CO2_WWF":980,	"BioDiffPercentage":-10,"DiffCoolingAbsolute":0,"DiffConservationAbsolute":750},
"kartoffeln (frisch)":			{"parent":"-","Foodtype":0,"CO2_Oeko":182,	"CO2_Pendos":200,	"CO2_WWF":620,	"BioDiffPercentage":-25,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},
"gemuese (frisch)":			{"parent":"-","Foodtype":0,"CO2_Oeko":137,	"CO2_Pendos":150,	"CO2_Wiegmann":150,	"CO2_WWF":900,	"BioDiffPercentage":-15,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},
"honig":				{"parent":"-","Foodtype":1,"CO2_Eigene_Schaetzung":0,		"CO2_WWF":0,	"BioDiffPercentage":0,"DiffCoolingAbsolute":0,"DiffConservationAbsolute":0},
"fisch":				{"parent":"-","Foodtype":3,"CO2_Eigene_Schaetzung":3500,	"CO2_WWF":4120, "BioDiffPercentage":-13,"DiffCoolingAbsolute":250,"DiffConservationAbsolute":350},#Schaetzung mit tk / konserve von anderen
"getreide":				{"parent":"-","Foodtype":0,"CO2_Eigene_Schaetzung":450,		"CO2_WWF":1680, "BioDiffPercentage":-25},
"reis":					{"parent":"-","Foodtype":0,"CO2_Eigene_Schaetzung":4000,	"CO2_WWF":6200, "BioDiffPercentage":-15},
"kartoffelstaerke":			{"parent":"-","Foodtype":0,"CO2_Eigene_Schaetzung":3000,	"CO2_WWF":3120, "BioDiffPercentage":-5},
"nuesse":				{"parent":"-","Foodtype":0,"CO2_Eigene_Schaetzung":800,		"CO2_WWF":1770, "BioDiffPercentage":-10},
"trockenobst":				{"parent":"-","Foodtype":0,"CO2_Eigene_Schaetzung":2400,	"CO2_WWF":3120, "BioDiffPercentage":-4},
"schaf&ziegenfleisch":			{"parent":"-","Foodtype":3,"CO2_Eigene_Schaetzung":8000,	"CO2_WWF":14900,"BioDiffPercentage":-15,"DiffCoolingAbsolute":850},
"milchpulver":				{"parent":"-","Foodtype":1,"CO2_Eigene_Schaetzung":12000,	"CO2_WWF":14700},
"wasser":				{"parent":"-","Foodtype":0,"CO2_Oeko":0},
}
firstfactors={#faktoren mit dazugehörigen zahlen als beispiel hab ich hier einige gelassen
"fettgehalt":{"replace_by":""},
"% f.i.t.":{"replace_by":"","direction":"l"},
}
factortexts={"mit ":{"replace_by":""},#faktoren ohne dazugehörige zahlen, hier auch nur einige beispiele
"aus oekologischem anbau":{"replace_by":"","meansfactor":"bio"},
"aus eu-/ nicht-eu-landwirtschaft":{"replace_by":"","meanstransport":"europa"},
"fairtrade":{"replace_by":"","meanstype":"fairtrade"},
"auf pflanzlicher basis":{"replace_by":"","meanstype":"vegan"},
"einfach konzentriertes":{"relevant_factor_in":{"default":2.5,"milch":10,"obst (mix)":10,"gemuese (frisch)":5},"updated_by":["saft aus","saefte aus"],"replace_by":""},
"thueringer":{"replace_by":"","meanstransport":"regional"},
"franzoesischer":{"replace_by":"","meanstransport":"europa"},
"japanischer":{"replace_by":"","meanstransport":"schiff"},
"frittierter":{"relevant_in":{"default":2000},"replace_by":""},
}
transportfactors={"regional":230,
"europa":460,
"schiff":570,
"flugzeug":11000
}
packagingfactors={
"frischgemuese papier": 10,
"frischgemuese kunststoffschale":25,
"konserven weissblechdose":105,
"konserven einwegglas": 190,
"tk kunststofffolie": 60,
"1,5l pet":82,
"1,5l leichte pet (ohne kohlensaeure)":88,
"1l pet":112,
"1l leichtglasflasche":230,
"0,5l pet":198,
"0,5l aluminiumdose":211,
"0,5l weissblechdose":365,
"1l mehrwegglas": 56,
"0,5l mehrwegglas": 98,
"0,25l mehrwegglas": 140,
"0,5l mehrwegpet": 105,
}
lastzutat=""

notavaliable={}
def getParents(name):
    parents=[]
    if(name in Network and Network[name]["parent"]!="-"):
        parents.append(Network[name]["parent"])
        toappend=getParents(Network[name]["parent"])
        for appendable in toappend:
            parents.append(appendable)
    return parents
def getco2valknown(name,listin={}):
    #print(name)
    #print(Network)
    nametmp=name.strip('\n').lower()
    co2val={"CO2":-1, "Reason":"", "Foodtype":-1,"BioDiffPercentage":-1,"SeasonDiffAbsolute":-1,"SeasonBegin":-1,"SeasonEnd":-1,"DiffCoolingAbsolute":-1,"DiffConservationAbsolute":-1}
    if listin!={}:
        co2val=listin
    #if not nametmp in Network:
    #    zgmenu("",nametmp,False)
    #print(Network[nametmp])
    if(co2val["CO2"]==-1 and "CO2_Eigene_Schaetzung" in Network[nametmp] and Network[nametmp]["CO2_Eigene_Schaetzung"]!=-1):
        co2val["CO2"]=Network[nametmp]["CO2_Eigene_Schaetzung"]
        co2val["Reason"]=" ist wohl "+name+" und das nach eigener Schätzung ist "+str(Network[nametmp]["CO2_Eigene_Schaetzung"])
    elif(co2val["CO2"]==-1 and "CO2_Oeko" in Network[nametmp] and Network[nametmp]["CO2_Oeko"]!=-1):
        co2val["CO2"]=Network[nametmp]["CO2_Oeko"]
        co2val["Reason"]=" ist wohl "+name+" und das nach Oekoinstitut ist "+str(Network[nametmp]["CO2_Oeko"])
    elif(co2val["CO2"]==-1 and "CO2_Pendos" in Network[nametmp] and Network[nametmp]["CO2_Pendos"]!=-1):
        co2val["CO2"]=Network[nametmp]["CO2_Pendos"]
        co2val["Reason"]=" ist wohl "+name+" und das nach CO2Zähler von Pendos ist "+str(Network[nametmp]["CO2_Pendos"])
    elif(co2val["CO2"]==-1 and "CO2_WWF" in Network[nametmp] and Network[nametmp]["CO2_WWF"]!=-1):
        co2val["CO2"]=Network[nametmp]["CO2_WWF"]
        co2val["Reason"]=" ist wohl "+name+" und das nach WWF Tabelle ist "+str(Network[nametmp]["CO2_WWF"])
    if co2val["Foodtype"]==-1 and "Foodtype" in Network[nametmp]:
        co2val["Foodtype"]=Network[nametmp]["Foodtype"]
    if co2val["BioDiffPercentage"]==-1 and "BioDiffPercentage" in Network[nametmp]:
        co2val["BioDiffPercentage"]=Network[nametmp]["BioDiffPercentage"]
    if co2val["SeasonDiffAbsolute"]==-1 and "SeasonDiffAbsolute" in Network[nametmp]:
        co2val["SeasonDiffAbsolute"]=Network[nametmp]["SeasonDiffAbsolute"]
    if co2val["SeasonBegin"]==-1 and "SeasonBegin" in Network[nametmp]:
        co2val["SeasonBegin"]=Network[nametmp]["SeasonBegin"]
    if co2val["SeasonEnd"]==-1 and "SeasonEnd" in Network[nametmp]:
        co2val["SeasonEnd"]=Network[nametmp]["SeasonEnd"]
    if co2val["DiffCoolingAbsolute"]==-1 and "DiffCoolingAbsolute" in Network[nametmp]:
        co2val["DiffCoolingAbsolute"]=Network[nametmp]["DiffCoolingAbsolute"]
    if co2val["DiffConservationAbsolute"]==-1 and "DiffConservationAbsolute" in Network[nametmp]:
        co2val["DiffConservationAbsolute"]=Network[nametmp]["DiffConservationAbsolute"]
    if(Network[nametmp]["parent"]!="-"):
        co2val=getco2valknown(Network[nametmp]["parent"],co2val)
    currentmonth=datetime.today().month-1
    if(co2val["SeasonDiffAbsolute"]>0 and co2val["SeasonBegin"]>=0 and co2val["SeasonEnd"]>=0):
        diff=0
        if(co2val["SeasonBegin"]<=co2val["SeasonEnd"] and currentmonth>co2val["SeasonEnd"]):
            diff=currentmonth-co2val["SeasonEnd"]
        if(co2val["SeasonBegin"]<=co2val["SeasonEnd"] and currentmonth<co2val["SeasonBegin"]):
            diff=currentmonth+12-co2val["SeasonEnd"]
        if(co2val["SeasonBegin"]>co2val["SeasonEnd"] and currentmonth>co2val["SeasonEnd"] and  currentmonth<co2val["SeasonBegin"]):
            diff=currentmonth-co2val["SeasonEnd"]
        possiblemonths=12
        if(co2val["SeasonBegin"]<co2val["SeasonEnd"]):
            possiblemonths=co2val["SeasonEnd"]-co2val["SeasonBegin"]
        elif(co2val["SeasonBegin"]>co2val["SeasonEnd"]):
            possiblemonths=co2val["SeasonEnd"]+12-co2val["SeasonBegin"]
        co2val["CO2SeasonFactor"]=(float(diff)/float(possiblemonths))*co2val["SeasonDiffAbsolute"]
            
    return co2val
def percentorempty(text):
    text=text.strip(":").strip("%").strip()
    if(text==""):
        return True
    try:
        float(text)
    except ValueError:
        return False
    return True
def replace_with_numbers(text,verbose=False):
    string=text.replace("*"," ")+" "
    for factor in sorted(firstfactors, key=len, reverse=True):
        startstring=string.find(factor)
        if(startstring!=-1):
            startnum=startstring+len(factor)
            numberwidthright=0
            if(verbose):
                print(string)
            while len(string)>startnum+numberwidthright and not ("direction" in firstfactors[factor] and  firstfactors[factor]["direction"]=="l"):
                if(percentorempty(string[startnum:startnum+numberwidthright])):
                    numberwidthright+=1
                else:
                    break
            if(verbose):
                print(string)
                print(numberwidthright)
            if(numberwidthright>2):
                string=string[:startstring]+" "+string[startnum+numberwidthright-1:]
                break
            numberwidthleft=0
            if(verbose):
                print(string)
            while 0<startstring-numberwidthleft:
                if(verbose):
                    print(string[startstring-numberwidthleft-1:startstring])
                if(percentorempty(string[startstring-numberwidthleft-1:startstring])):
                    numberwidthleft+=1
                else:
                    break
            if(verbose):
                print(string)
                print("here")
                print(startstring)
                print(factor)
                print(startnum)
                print(numberwidthleft)
            if(startstring>numberwidthleft and numberwidthleft>0):
                string=string[:startstring-numberwidthleft]+" "+string[startnum:]
            elif(numberwidthleft>0):
                string=string[:0]+" "+string[startnum:]
            if(verbose):
                print(string)
            #if(len(string)>startstring and string[startstring]=='%'):
            #    string=string[:startstring]+string[startstring+1:]
    #print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    return string

def autoreplace_factortexts(text,verbose=False):
    string=" "+text+" "
    #if(text.find("citrus-aromen")!=-1):
    #    print(string)
    #    time.sleep(3)
    meanstype=""
    meanstransport=""
    meanspackaging=-1
    relevant_in=[]
    relevant_factor_in=[]
    meansfactor=[]
    if(verbose):
        print(string)
#for k in sorted(d, key=len, reverse=True)
    for factor in sorted(factortexts, key=len, reverse=True):
        factortext=factortexts[factor]
        toreplace=""
        while "parent" in factortext:
            if(toreplace=="" and "replace_by" in factortext):
                toreplace=factortext["replace_by"]
            factortext=factortexts[factortexts[factor]["parent"]]
        stringtmp=string.replace(factor,toreplace)
        #if(text.find("citrus-aromen")!=-1 and stringtmp!=string):
        #    print(string)
        #    print(stringtmp)
        #    time.sleep(3)
        if(stringtmp!=string or ("updated_by" in factor and text.find(factor["updated_by"][0])!=-1 and text.find(factor["updated_by"][1])!=-1)):
            if(verbose):
                print(stringtmp)
            if meanstype!="" and "meanstype" in factortext:
                meanstype=factortext["meanstype"]
            if "relevant_in" in factortext:
                relevant_in.append(factortext["relevant_in"])
            if "relevant_factor_in" in factortext:
                relevant_factor_in.append(factortext["relevant_factor_in"])
            if "meanspackaging" in factortext:
                meanspackaging=int(factortext["meanspackaging"])
            if "meanstransport" in factortext:
                meanstransport=factortext["meanstransport"]
            if "meansfactor" in factortext:
                meansfactor.append(factortext["meansfactor"])
        if(stringtmp.strip()!=""):
            string=stringtmp
    #while(string.find("  ")!=-1):
    #    string[string.find("  ")]=''
    string=' '.join(string.split())
    result={"string":string}
    if meanstype!="":
        result["type"]=meanstype
    if relevant_in!=[]:
        result["relevant_in"]=relevant_in
    if relevant_factor_in!=[]:
        result["relevant_factor_in"]=relevant_factor_in
    if meanspackaging!=-1:
        result["meanspackaging"]=meanspackaging
    if meanstransport!="":
        result["meanstransport"]=meanstransport
    if meansfactor!=[]:
        result["meansfactor"]=meansfactor
    #if(text.find("citrus-aromen")!=-1):
    #    print(string)
    #    time.sleep(10)
    return result

def replacecommas_in_numbers(text):
    result=text
    commaposition=-1
    while(result.find(",",commaposition+1)!=-1):
        commaposition=result.find(",",commaposition+1)
        if(len(result)>commaposition+1 and commaposition!=0):
            if(result[commaposition-1].isnumeric() and result[commaposition+1].isnumeric()):
                result=result[:commaposition]+"."+result[commaposition+1:]
    return result
def widencommas_without_numbers(text):
    result=text
    commaposition=-1
    while(result.find(",",commaposition+1)!=-1):
        commaposition=result.find(",",commaposition+1)
        if(len(result)>commaposition+1 and commaposition!=0):
            if(not result[commaposition-1].isnumeric()and not result[commaposition+1].isnumeric()):
                result=result[:commaposition]+", "+result[commaposition+1:]
    return result

###Here everything happens###
#############################

def getCO2(ztliste,main=True,skip_new=True,iscompensated=False,verbose=False,actpackaging="",actregion="",isbio=False):
    #print(repr(ztliste))
    if(verbose):
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print(repr(ztliste))
    co2eq=0.0
    liste=ztliste#.split(". ")[0]
    if(liste.lower().find("zutaten:")!=-1):
        liste=liste[liste.lower().find("zutaten:")+8:]
    liste=widencommas_without_numbers(liste)
    zutatencombined=liste.replace('-,','-').replace('\n',' ').replace(';',',').replace(" ,",", ").replace("),","), ").replace('gewürztes,','').replace('gewürzter,','')
    zutatencombined=zutatencombined.lower().strip().replace("ü", "ue").replace("ä","ae").replace("ö","oe").replace("ß","ss")
    zutattmpreplaced=autoreplace_factortexts(zutatencombined)
    iscompensated=False
    if "meanstransport" in zutattmpreplaced:
        actregion=zutattmpreplaced["meanstransport"]
    if "meanspackaging" in zutattmpreplaced:
        maindiff=zutattmpreplaced["meanspackaging"]
    if "meansfactor" in zutattmpreplaced:
        for zttmprplfactor in zutattmpreplaced["meansfactor"]:
            if(verbose):
                print(zutattmpreplaced)
            if(zttmprplfactor=="bio"):
                isbio=True
            if(zttmprplfactor=="kompensiert"):
                iscompensated=True

    zutaten =zutatencombined.split(", ")
    zutatmods={'²':[],'¹':[],'³':[],'***':[],'**':[],'*':[]}
    zutatengenerated={}
    toremove=[]
    toskip=0
    for i in range(len(toremove)):
        #print(toremove[len(toremove)-1-i])
        zutaten.pop(toremove[-1])
        toremove.pop(-1)
    maindiff=0
    if(iscompensated):
        return 0
    for zutat in range(len(zutaten)):
        if(verbose):
            print("zutat "+zutaten[zutat])
        if(zutat+toskip)>=len(zutaten):
            break
        zutattmp=zutaten[zutat+toskip]
        zutattmp=zutattmp.lower().strip()#.replace("ü", "ue").replace("ä","ae").replace("ö","oe").replace("ß","ss")
        allergenstmp=["eier","soja","ei","e i","laktose","gluten","sesam", "haselnuessen", "sulfit", "schalen", "huenerei", "schafsmilch","mandel","erdnuss", "weizen", "schwefeldioxid", "nuss", "nuess", "nuessen", "haselnuss", "krustentieren", "pflaumenkernen", "rindfleisch","senf","cashew","lupinen","schalenfruechte","krabbe","krebstiere","magermilchpulver","magermilchpulver", "sulfite","anderen schalenfruechten","schalenfruechten","erdnuessen","sellerie","nuesse","milch"]
        zutattmp2=zutattmp.split("kann spuren von anderen")[0]
        zutattmp2=zutattmp2.split("kann spuren von weiteren")[0]
        zutattmp2=zutattmp2.split("kann spuren von ")[0]
        for allergentmp in sorted(allergenstmp, key=len, reverse=True):
            zutattmp2=zutattmp2.split("kann "+allergentmp)[0]
            zutattmp2=zutattmp2.split("enthaelt "+allergentmp)[0]
        if(zutattmp2.find("kann spuren von")!=-1):
            print(zutattmp2.split("enthaelt")[1])
        if(zutattmp2!=zutattmp):
            toskip=len(zutaten)#ich glaube dann sind wir durch mit den zutateninformationen
        zutattmp=replacecommas_in_numbers(zutattmp)
        zutattmp=replace_with_numbers(zutattmp)
        zutattmpreplaced=autoreplace_factortexts(zutattmp)
        zutattmp=zutattmpreplaced["string"]
        if(verbose):
            print(zutattmpreplaced)
        #print(zutattmp)
        zutatco2eq=0.0
        percentage=-1.0
        while(zutattmp.find('[')>=0):
            countbrk=1
            pos1=zutattmp.find('[')+1
            pos2=zutattmp.find('[')+1
            while(countbrk>0):
                if(zutattmp.find('[',pos2)>=0 and zutattmp.find('[',pos2)<zutattmp.find(']',pos2)):
                    pos2=zutattmp.find('[',pos2)+1
                    countbrk+=1
                elif(zutattmp.find(']',pos2)>=0 and (zutattmp.find('[',pos2)>zutattmp.find(']',pos2) or zutattmp.find('[',pos2)==-1)):
                    pos2=zutattmp.find(']',pos2)+1
                    countbrk-=1
                else:
                    if(zutattmp.find(']',pos2)==-1):
                        if(zutat+toskip+1<len(zutaten)):
                            toskip+=1
                            addzutat=zutaten[zutat+toskip]
                            addzutat=addzutat.lower().strip().replace("ü","ue").replace("ä","ae").replace("ö","oe").replace("ß","ss")
                            addzutat=replacecommas_in_numbers(addzutat)
                            addzutat=replace_with_numbers(addzutat)
                            addzutatreplaced=autoreplace_factortexts(addzutat)
                            addzutat=addzutatreplaced["string"]
                            zutattmp=zutattmp+", "+addzutat
                        else:
                            pos2=len(zutattmp)
                            break
            #if(zutattmp[pos1:pos2-1])
            tosearch=zutattmp[pos1:pos2]
            if(tosearch[-1:]=="]"):
                tosearch=tosearch[:-1]
            zutattmp=zutattmp[:pos1-1]+zutattmp[pos2:]
            zutatengenerated[zutattmp]=[False,{"CO2":getCO2(tosearch.strip(),main=False,actpackaging=actpackaging,actregion=actregion,isbio=isbio,skip_new=skip_new)},0.0]
        while(zutattmp.find('{')>=0):
            countbrk=1
            pos1=zutattmp.find('{')+1
            pos2=zutattmp.find('{')+1
            while(countbrk>0):
                if(zutattmp.find('{',pos2)>=0 and zutattmp.find('{',pos2)<zutattmp.find('}',pos2)):
                    pos2=zutattmp.find('{',pos2)+1
                    countbrk+=1
                elif(zutattmp.find('}',pos2)>=0 and (zutattmp.find('{',pos2)>zutattmp.find('}',pos2) or zutattmp.find('{',pos2)==-1)):
                    pos2=zutattmp.find('}',pos2)+1
                    countbrk-=1
                else:
                    if(zutattmp.find('}',pos2)==-1):
                        if(zutat+toskip+1<len(zutaten)):
                            toskip+=1
                            addzutat=zutaten[zutat+toskip]
                            addzutat=addzutat.lower().strip().replace("ü","ue").replace("ä","ae").replace("ö","oe").replace("ß","ss")
                            addzutat=replacecommas_in_numbers(addzutat)
                            addzutat=replace_with_numbers(addzutat)
                            addzutatreplaced=autoreplace_factortexts(addzutat)
                            addzutat=addzutatreplaced["string"]
                            zutattmp=zutattmp+", "+addzutat
                        else:
                            pos2=len(zutattmp)
                            break
            #if(zutattmp[pos1:pos2-1])
            tosearch=zutattmp[pos1:pos2]
            if(tosearch[-1:]=="}"):
                tosearch=tosearch[:-1]
            zutattmp=zutattmp[:pos1-1]+zutattmp[pos2:]
            zutatengenerated[zutattmp]=[False,{"CO2":getCO2(tosearch.strip(),main=False,actpackaging=actpackaging,actregion=actregion,isbio=isbio,skip_new=skip_new)},0.0]
            #print(zutattmp)
        while(zutattmp.find('(')>=0):
            #print(zutattmp)
            countbrk=1
            pos1=zutattmp.find('(')+1
            pos2=zutattmp.find('(')+1
            while(countbrk>0):
                if(zutattmp.find('(',pos2)>=0 and zutattmp.find('(',pos2)<zutattmp.find(')',pos2)):
                    pos2=zutattmp.find('(',pos2)+1
                    countbrk+=1
                elif(zutattmp.find(')',pos2)>=0 and (zutattmp.find('(',pos2)>zutattmp.find(')',pos2) or zutattmp.find('(',pos2)==-1)):
                    pos2=zutattmp.find(')',pos2)+1
                    countbrk-=1
                else:
                    if(zutattmp.find(')',pos2)==-1):
                        if(zutat+toskip+1<len(zutaten)):
                            toskip+=1
                            addzutat=zutaten[zutat+toskip]
                            addzutat=addzutat.lower().strip().replace("ü","ue").replace("ä","ae").replace("ö","oe").replace("ß","ss")
                            addzutat=replacecommas_in_numbers(addzutat)
                            addzutat=replace_with_numbers(addzutat)
                            addzutatreplaced=autoreplace_factortexts(addzutat)
                            addzutat=addzutatreplaced["string"]
                            zutattmp=zutattmp+", "+addzutat
                        else:
                            pos2=len(zutattmp)
                            break
            #if(zutattmp[pos1:pos2-1])
            tosearch=zutattmp[pos1:pos2]
            #print("searching : "+tosearch)
            if(tosearch[-1:]==")"):
                tosearch=tosearch[:-1]
            zutattmp=zutattmp[:pos1-1]+zutattmp[pos2:]
            #print("searching : "+tosearch)
            if(tosearch.strip()[-1:]=="%"):
                try:
                    percentage=float(tosearch.replace(",",".").strip()[:-1])
                except ValueError:
                    #print("searching : "+tosearch)
                    zutatengenerated[zutattmp]=[False,{"CO2":getCO2(tosearch.strip(),main=False,actpackaging=actpackaging,actregion=actregion,isbio=isbio,skip_new=skip_new)},0.0]
                    if(verbose):
                        print(zutatengenerated)
            else:
                #print("searching : "+tosearch)
                zutatengenerated[zutattmp]=[False,{"CO2":getCO2(tosearch.strip(),main=False,actpackaging=actpackaging,actregion=actregion,isbio=isbio,skip_new=skip_new)},0.0]
        zutattmp=zutattmp.replace(", "," ").replace("|"," ").replace(": "," ").replace(". "," ").replace("/n"," ").strip()
        zutattmp=' '.join(zutattmp.split())
        if(zutattmp.find('¹')!=-1):
            if(len(zutattmp)>zutattmp.find('¹') and zutattmp.find('¹')!=0):
                zutattmp=zutattmp[:zutattmp.find('¹')]
            zutattmp=zutattmp.replace('¹','')
            zutatmods['¹'].append(zutattmp)
        if(zutattmp.find('²')!=-1):
            if(len(zutattmp)>zutattmp.find('²') and zutattmp.find('²')!=0):
                zutattmp=zutattmp[:zutattmp.find('²')]
            zutattmp=zutattmp.replace('²','')
            zutatmods['²'].append(zutattmp)
        if(zutattmp.find('³')!=-1):
            if(len(zutattmp)>zutattmp.find('³') and zutattmp.find('³')!=0):
                zutattmp=zutattmp[:zutattmp.find('³')]
            zutattmp=zutattmp.replace('³','')
            zutatmods['³'].append(zutattmp)
        if(zutattmp.find("***")!=-1):
            if(len(zutattmp)>zutattmp.find("***") and zutattmp.find('***')!=0):
                zutattmp=zutattmp[:zutattmp.find('***')]
            zutattmp=zutattmp.replace("***"," ")
            zutatmods["***"].append(zutattmp)
        if(zutattmp.find("**")!=-1):
            if(len(zutattmp)>zutattmp.find("**") and zutattmp.find('**')!=0):
                zutattmp=zutattmp[:zutattmp.find('**')]
            zutattmp=zutattmp.replace("**"," ")
            zutatmods["**"].append(zutattmp)
        if(zutattmp.find('*')!=-1):
            if(len(zutattmp)>zutattmp.find("*") and zutattmp.find('*')!=0):
                zutattmp=zutattmp[:zutattmp.find('*')]
            zutattmp=zutattmp.replace('*',' ')
            zutatmods['*'].append(zutattmp)
        oldzutattmp=zutattmp
        zutattmp=replace_with_numbers(zutattmp)
        if(zutattmp.find("%")!=-1):
            zutattmp=zutattmp.replace(" %","%")
            percenttmp=zutattmp.split("%")[0].strip()
            if(percenttmp.find(" ")!=-1):
                percenttmp=percenttmp.split(" ")[-1]
            percenttmp=percenttmp.replace(",",".")
            zutattmp=zutattmp.replace(",",".")
            try:
                #print("trying to convert this:")
                #print(percenttmp)
                percentage=float(percenttmp)
            except ValueError:
                tmp=0.0
                for i in range(1,len(percenttmp)):
                    try:
                        tmp=float(percenttmp[-i:])
                        percenttmp=percenttmp[-i:]
                    except ValueError:
                        break
                percentage=tmp
                #print("VALUEERROR: "+zutattmp+" is"+str(tmp))
            zutattmp=zutattmp.replace(percenttmp,"")
            zutattmp=zutattmp.replace("%","")
        if(zutattmp.lower().find("kann spuren")!=-1):
            zutattmp=zutattmp[:zutattmp.lower().find("kann spuren")].strip().strip(".")
            break
        if(zutattmp.lower().find("kann enthalten")!=-1):
            zutattmp=zutattmp[:zutattmp.lower().find("kann enthalten")].strip().strip(".")
            break
        if(zutattmp.lower().find("enthaelt")!=-1):
            zutattmp=zutattmp[:zutattmp.lower().find("enthaelt")].strip().strip(".")
            break
        if(zutattmp.lower().find("spuren")!=-1):
            zutattmp=zutattmp[:zutattmp.lower().find("spuren")].strip().strip(".")
            break
        if(zutattmp.lower().find("hinweis")!=-1):
            zutattmp=zutattmp[:zutattmp.lower().find("hinweis")].strip().strip(".")
            break
        if(oldzutattmp in zutatengenerated and not zutattmp==oldzutattmp):
            if(verbose):
                print(zutattmp)
                print(oldzutattmp)
                print(zutatengenerated)
            zutatengenerated[zutattmp]=zutatengenerated[oldzutattmp]
            del zutatengenerated[oldzutattmp]
        zutattmp=zutattmp.strip().strip(",").strip(":").strip(".").strip().strip(":").strip("-").strip(".").strip("]").strip("[").strip(")").strip("(").strip("<").strip(">").strip().strip('\n').lower()
        if(zutattmp in Network and not Network[zutattmp]["parent"]=="quatsch"):
            if(percentage<=0.0):
                zutatengenerated[zutattmp]=[False,{},0.0]
            else:
                zutatengenerated[zutattmp]=[True,{},percentage]
        #print(zutattmp)
        if(verbose):
            print(zutatengenerated)
        if(zutattmp in Network):
            if(not Network[zutattmp]["parent"]=="quatsch"):
                zutatengenerated[zutattmp][1]=getco2valknown(zutattmp)
                zutatengenerated[zutattmp][1]["Reason"]=zutattmp+zutatengenerated[zutattmp][1]["Reason"]
        elif(zutattmp!=''):
            if(skip_new):
                if(zutattmp in ToDealWith):
                    ToDealWith[zutattmp]=ToDealWith[zutattmp]+1
                else:
                    ToDealWith[zutattmp]=1
                    ToDealWithTexts[zutattmp]=zutaten
                if(zutattmp in Network and not Network[zutattmp]["parent"]=="quatsch"):
                    zutatengenerated[zutattmp][1]={"CO2":15000,"Reason":zutattmp+" not found, defaulted to 15000"}
            else:
                if(verbose):
                    printlines("",12)
                co2valuestmp=zgmenu(zutaten,zutattmp)
                if(zutattmp in zutatengenerated):
                    zutatengenerated[zutattmp][1]=co2valuestmp
        #zutatengenerated[zutattmp][1]["relevant_in"]=zutattmpreplaced["relevant_in"]
        #zutatengenerated[zutattmp][1]["relevant_factor_in"]=zutattmpreplaced["relevant_factor_in"]
        #zutatengenerated[zutattmp][1]["meanspackaging"]=zutattmpreplaced["meanspackaging"]
        if(zutattmp in zutatengenerated and zutattmp in Network and not Network[zutattmp]["parent"]=="quatsch"):
            multiplyfactor=1.0
            if isbio:
                parentstmp=getParents(zutattmp)
                for parent in parentstmp:
                    if("BioDiffPercentage" in Network[parent]):
                        toadddiff=float(Network[parent]["BioDiffPercentage"])/100.0
                        if(toadddiff>0.0):
                            toadddiff*=-1.0
                        multiplyfactor+=toadddiff
                        #print("bio factor: "+str(multiplyfactor))
            if "relevant_factor_in" in zutattmpreplaced:
                for factor in zutattmpreplaced["relevant_factor_in"]:
                    for parentkey in factor:
                        if parentkey in getParents(zutattmp) and not parentkey=="default":
                            multiplyfactor=multiplyfactor+factor[parentkey]-1.0
                            #zutatengenerated[zutattmp][1]["CO2"]=zutatengenerated[zutattmp][1]["CO2"]*factor[parentkey]
                            #zutatengenerated[zutattmp][1]["Reason"]=" is part of the "+factor+" group for being in "+parentkey+" and "+zutatengenerated[zutattmp][1]["Reason"]
                            break
                    if("default" in factor):
                        multiplyfactor=multiplyfactor+factor["default"]-1.0
            if not "CO2" in zutatengenerated[zutattmp][1]:#,actpackaging="",actregion="",isbio=False
                zutatengenerated[zutattmp][1]["CO2"]=1000
                print("DECEMATED "+zutat)
            zutatengenerated[zutattmp][1]["CO2"]=zutatengenerated[zutattmp][1]["CO2"]*multiplyfactor
            if not main or Network[zutattmp]["parent"]=="quatsch":
                pass
            elif actpackaging=="unverpackt":
                zutatengenerated[zutattmp][1]["CO2"]+=30
            elif actpackaging=="pfand":
                zutatengenerated[zutattmp][1]["CO2"]+=150
            else:
                zutatengenerated[zutattmp][1]["CO2"]+=300
                #print("unverpackt")
            if not main or Network[zutattmp]["parent"]=="quatsch":
                pass
                #print("heeere")
            elif actregion=="regional":
                zutatengenerated[zutattmp][1]["CO2"]+=230
            elif actregion=="europa":
                zutatengenerated[zutattmp][1]["CO2"]+=460
            else:
                zutatengenerated[zutattmp][1]["CO2"]+=570
                #print("mittem schiff")
            if "relevant_in" in zutattmpreplaced:
                for factor in zutattmpreplaced["relevant_in"]:
                    for parentkey in factor:
                        if parentkey in getParents(zutattmp) and not parentkey=="default":
                            if(verbose):
                                print(factor[parentkey])
                            zutatengenerated[zutattmp][1]["CO2"]=zutatengenerated[zutattmp][1]["CO2"]+factor[parentkey]
                            #zutatengenerated[zutattmp][1]["Reason"]=" is part of the "+factor+" group for being in "+parentkey+" and "+zutatengenerated[zutattmp][1]["Reason"]
                            break
                    if("default" in factor):
                         zutatengenerated[zutattmp][1]["CO2"]=zutatengenerated[zutattmp][1]["CO2"]+factor["default"]
            #if "meanspackaging" in zutattmpreplaced:
                #zutatengenerated[zutattmp][1]["CO2"]=zutatengenerated[zutattmp][1]["CO2"]+zutattmpreplaced["meanspackaging"]
                    #zutatengenerated[zutattmp][1]["Reason"]=" is part of the "+factor+" group for being in "+parentkey+" and "+zutatengenerated[zutattmp][1]["Reason"]
#                    break
        #print(zutattmp)
        #if(zutattmp.find("29")!=-1):
        #    print(zutat)
    if(verbose):
        print(zutaten)
    if(len(zutatengenerated)==1):
        zutatengenerated[list(zutatengenerated.keys())[0]][2]=100.0
    else:
        while True:
            if(verbose):
                print()
            ztgkeys=list(zutatengenerated.keys())
            topvalnum=-1#der wert der am meisten prozente haben sollte um möglichst viel CO2 zu produzieren (weil worstcase)
            topvalCO2=0#der
            ztlisttmp=[]
            #print(zutatengenerated)
            for zutat in zutatengenerated:
                if(verbose):
                    print(zutatengenerated[zutat][0],zutatengenerated[zutat][2])
                if(not "CO2" in zutatengenerated[zutat][1]):
                     zutatengenerated[zutat][0]=True
                else:
                    if(not zutatengenerated[zutat][0]):
                        CO2tmp=zutatengenerated[zutat][1]["CO2"]#Der CO2 Wert der zutat
                        if(main and "CO2SeasonFactor" in zutatengenerated[zutat][1]):
                            CO2tmp+=zutatengenerated[zutat][1]["CO2SeasonFactor"]
                        tobeatfactor=0.0 #Der CO2 Wert den die nächste zutat "Überbieten" müsste um topvalnum zu werden
                        for ztsub in ztlisttmp:
                            tobeatfactor+=zutatengenerated[ztsub][1]["CO2"]/len(ztlisttmp)
                            if(main and "CO2SeasonFactor" in zutatengenerated[ztsub][1]):
                                tobeatfactor+=zutatengenerated[ztsub][1]["CO2SeasonFactor"]/len(ztlisttmp)
                        ztlisttmp.append(zutat)
                        if(tobeatfactor<CO2tmp or (CO2tmp==0 and topvalCO2==0)):# 4 values: if value4/4>value3/(3*4)+value2/(3*4)+value1/(3*4)
                            topvalnum=ztgkeys.index(zutat)
                            topvalCO2=CO2tmp
                    else:
                        ztlisttmp=[]#abschnitte werden durch bereits festgelegte prozente markiert und getrennt betrachtet
            if(topvalnum==-1):
                break
            oberschranke=100.0#die maximalen prozente die ein wert erreichen kann
            unterschranke=0.0#die minimalen prozente die ein wert haben muss
            count_empty=0#
            #actcount=0
            countabove=0
            countunknown=0
            numberbelow=0
            lastfound=-1
            allready_gone=0.0
            for zutat in zutatengenerated:
                if("CO2" in zutatengenerated[zutat][1]):
                    if(zutatengenerated[zutat][0]):#wenn zutatenwerte vergeben wurden
                        if(topvalnum>ztgkeys.index(zutat)):#links von dem wert der am höchsten sein sollte
                            oberschranke=zutatengenerated[zutat][2]
                            allready_gone+=zutatengenerated[zutat][2]*(countunknown+1)
                            countunknown=0
                            lastfound=ztgkeys.index(zutat)
                        elif unterschranke==0.0:#rechts davon wird abgezogen, erster fester ist unterschranke
                            unterschranke=zutatengenerated[zutat][2]
                            #oberschranke-=zutatengenerated[zutat][2]
                            allready_gone+=zutatengenerated[zutat][2]*(countunknown+1)
                            countunknown=0
                            #actcount=count
                        else:
                            #oberschranke-=zutatengenerated[zutat][2]
                            allready_gone+=zutatengenerated[zutat][2]*(countunknown+1)
                            countunknown=0
                    else:
                        #count+=1
                        if(ztgkeys.index(zutat)==topvalnum):
                            countabove=(ztgkeys.index(zutat)-lastfound)-1
                            countunknown=0
                        else:
                            countunknown+=1
                        
            if(verbose):
                print("oberschranke ",oberschranke)
                #print("unterschranke ",unterschranke)
                print("100-gone: ",100.0-allready_gone)
                print("min_after ",countabove)
            #actoberschranke=oberschranke/actcount
            zutatengenerated[ztgkeys[topvalnum]][2]=min((100.0-allready_gone),oberschranke)/(countabove+1)
            zutatengenerated[ztgkeys[topvalnum]][0]=True
    actCO2=0
    countactual=0
    for actzutat in zutatengenerated:
        if(not "quatsch" in getParents(actzutat.strip())):
            countactual+=1
    if(countactual>1):
        actCO2+=100#for mixing and preparing
    for zutat in zutatengenerated:
        if("CO2" in zutatengenerated[zutat][1]):
            tmpCO2=zutatengenerated[zutat][1]["CO2"]
            if(main and "CO2SeasonFactor" in zutatengenerated[zutat][1]):
                tmpCO2+=zutatengenerated[zutat][1]["CO2SeasonFactor"]
            actCO2+=float(tmpCO2)*(zutatengenerated[zutat][2]/100.0)
        else:
            actCO2+=25000.0*(zutatengenerated[zutat][2]/100.0)
    if(verbose):
        print(zutatengenerated,actCO2)
    if(verbose and (actCO2<-1 or actCO2>40000)):
        for zutat in zutatengenerated:
            if("CO2" in zutatengenerated[zutat][1]):
                print(str(zutatengenerated[zutat][2]) + "% is " + str(zutatengenerated[zutat][1]["CO2"]))
        #time.sleep(10)
    return round(actCO2+maindiff)
    #print(
    #print("\n")
            #print()
            #print("IN: "+zutat[zutat.find('(')+1:].split('%')[0])
            #if(zutat[zutat.find('(')+1:].split('%')[0].find('(')>0):
            #print(zutat)
            #print("\n")
def writeJSONFile():
    NetworkFile=open("Products.json","w")
    NetworkFile.seek(0)
    jsonstring=json.dumps(Network,indent=4)
    NetworkFile.write(jsonstring)
    NetworkFile.truncate()
    NetworkFile.close()
    NetworkFile=open("Factors.json","w")
    NetworkFile.seek(0)
    jsonstring=json.dumps({"factortexts":factortexts,"firstfactors":firstfactors},indent=4)
    NetworkFile.write(jsonstring)
    NetworkFile.truncate()
    NetworkFile.close()
def readJSONFile():
    NetworkFile=open("Products.json","r")
    jsonstring=NetworkFile.read()
    NetworkFile.close()
    inputjson=json.loads(jsonstring)
    for inputjsonelem in inputjson:
        if("Foodtype" in inputjson[inputjsonelem]):
            if(inputjson[inputjsonelem]["Foodtype"]>10):
                inputjson[inputjsonelem]["CO2_Eigene_Schätzung"]=inputjson[inputjsonelem]["Foodtype"]
                inputjson[inputjsonelem]["Foodtype"]=4
                #print(inputjsonelem)
        if not inputjsonelem in Network:
            Network[inputjsonelem]=inputjson[inputjsonelem]
    
    NetworkFile=open("Factors.json","r")
    jsonstring=NetworkFile.read()
    NetworkFile.close()
    inputjson=json.loads(jsonstring)
    #print(inputjson)
    factortexts=inputjson["factortexts"]
    firstfactors=inputjson["firstfactors"]
    #Network=inputjson
    
def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
def deletelines(num=1):
    for _ in range(num):
        sys.stdout.write('\033[F')
        sys.stdout.write('\033[K')
def printlines(text="",num=1,color=""):
    acttext=text
    if(color=="g"):
        acttext="\033[1;32;40m "+acttext+"\033[1;37;40m"
    elif(color=="b"):
        acttext="\033[1;34;40m "+acttext+"\033[1;37;40m"
    elif(color==""):
        acttext="\033[1;37;40m "+acttext+"\033[1;37;40m"
    else:
        pos1=acttext.lower().find(color)
        acttext="\033[1;37;40m"+acttext[:pos1]+"\033[1;30;42m"+acttext[pos1:]
        pos2=acttext.lower().find(color)+len(color)
        acttext=acttext[:pos2]+"\033[1;37;40m"+acttext[pos2:]
    columns = int(os.popen('stty size', 'r').read().split()[1])
    for i in range(num):
        print(acttext[columns*i:columns*(i+1)])

def ridoflist(string,charlist):
    for elem in charlist:
        string=string.replace(elem,"")
    return string
def typocheck(string1,string2):
    uninterestingchars=[" ","-"]
    str1tmp=ridoflist(string1,uninterestingchars)
    str2tmp=ridoflist(string2,uninterestingchars)
    if(len(str1tmp)-1>len(str2tmp) or len(str2tmp)-1>len(str1tmp)):
        return False
    if(any(char.isdigit() for char in str1tmp) or any(char.isdigit() for char in str2tmp)):
        return False
    if(len(str1tmp)<4 or len(str2tmp)<4 or str1tmp.find("vit")!=-1 or str2tmp.find("vit")!=-1 or str1tmp.find("coco")!=-1 or str1tmp.find("rind")!=-1 or str1tmp.find("melisse")!=-1 or str1tmp.find("melasse")!=-1):
        return False
    #check for amount of wrongchars from both sides
    error1=-1
    error2=-1
    for i in range(len(str1tmp)):
        if(len(str2tmp)==i):
            return True
        if(str1tmp[i]!=str2tmp[i]):
            error1=i
            if(str1tmp[:error1]+str1tmp[error1+1:]==str2tmp or str2tmp[:error1]+str2tmp[error1+1:]==str1tmp):
                return True
            break
    for i in range(len(str2tmp)):
        if(len(str1tmp)==i):
            return True
        if(str1tmp[-i-1]!=str2tmp[-i-1]):
            error2=len(str1tmp)-i-1
            break
    if(error1==error2):
        print("missingchar: "+str1tmp+"/"+str2tmp+" "+str(error1)+"/"+str(error2))
        printlines("",20)
        return True #ein falscher char
    if(error1+1==error2 or error1==error2+1):
        if(len(str2tmp)==error2 or str1tmp[error1]==str2tmp[error2] and str1tmp[error2]==str2tmp[error1]):
            printlines("",20)
            print("charmoved: "+str1tmp+"/"+str2tmp+" "+str(error1)+"/"+str(error2))
            return True #char verschoben oder gelöscht
    return False
def findfactorizedparent(item):
    result=["",[]]
    return result



#printlines("",12)



def createzutatmenu(item):
    #antworten=["-",-1,-1,0,-1,-1,-1,-1,-1]
    antworten={"parent":"-","Foodtype":-1,"CO2_Eigene_Schaetzung":-1,"BioDiffPercentage":-1,"SeasonDiffAbsolute":-1,"SeasonBegin":-1,"SeasonEnd":-1,"DiffCoolingAbsolute":-1,"DiffConservationAbsolute":-1}
    antwortenstr=["parent","Foodtype","CO2_Eigene_Schaetzung","BioDiffPercentage","SeasonDiffAbsolute","SeasonBegin","SeasonEnd","DiffCoolingAbsolute","DiffConservationAbsolute"]
    fragen=["Hat es eine Obergruppe? (enter wenn nein)",
"ist es vegan(0), vegetarisch(1), unklar(2), fleisch(3), kuenstliches kleinteil(4), allergen(5)",
"wie viel CO2 produziert es grob? (enter wenn -1 (parent))",
"Wie viel abweichung hat ein Bioprodukt?",
"Wie viel abweichung ausserhalb der Saison?",
"Wann fängt die Saison an (januar(0),februar(1),märz(2),april(3)...)",
"Wann hört die Saison auf (januar(0),februar(1),märz(2),april(3)...)",
"Wie viel verbraucht es beim Kühlen?",
"Wie viel verbraucht es zu konservieren?"]
    fragenummer=0
    elements=[]
    selected=-1
    line=""
    answer=""
    deletelines(12)
    printlines("",4)
    printlines(debug,1,"b")
    printlines("",2)
    printlines(item,1,"g")
    printlines(str(antworten),1)
    printlines(fragen[fragenummer],1)
    printlines("",2)
    while True:
        ###input
        inputchar=readchar.readkey()
        if(inputchar=='\x03'):
            quit()
        elif(inputchar=='\t'):
            selected=selected+1%len(elements)
        elif(inputchar=='\r'):
            if(fragenummer==0):
                if(selected!=-1 and len(elements)>selected):
                    antworten["parent"]=elements[selected]
                    for parent in getParents(selected):
                        if("Foodtype" in parent):
                            antworten["Foodtype"]=Network[parent]["Foodtype"]
                            break
                    if("Foodtype" in antworten and (antworten["Foodtype"]==0 or antworten["Foodtype"]>2)):
                        fragenummer+=1
                elif(len(elements)==1):
                    antworten["parent"]=elements[0]
                    line=""
                    for parent in getParents(elements[0]):
                        if("Foodtype" in parent):
                            antworten["Foodtype"]=Network[parent]["Foodtype"]
                            break
                    if("Foodtype" in antworten and (antworten["Foodtype"]==0 or antworten["Foodtype"]>2)):
                        fragenummer+=1
                elif(len(elements)>1 and (line in elements)):
                    antworten["parent"]=line
                    for parent in getParents(line):
                        if("Foodtype" in parent):
                            antworten["Foodtype"]=Network[parent]["Foodtype"]
                            break
                    if("Foodtype" in antworten and (antworten["Foodtype"]==0 or antworten["Foodtype"]>2)):
                        fragenummer+=1
                    line=""
            else:
                if(line!=""):
                    antworten[antwortenstr[fragenummer]]=int(line)
                line=""
            fragenummer+=1
            if(fragenummer==2 and antworten["Foodtype"]==4):
                fragenummer=len(antwortenstr)
                antworten["CO2_Eigene_Schaetzung"]=15000
            if(fragenummer==5 and antworten["SeasonDiffAbsolute"]==0):
                fragenummer=7
            if(fragenummer==len(antwortenstr)):
                break
        elif(inputchar=='\x7f'):
            if(len(line)>0):
                line=line[:-1]
        else:
            line+=inputchar
        ###output
        deletelines(12)
        elements=[]
        listelem=""

        for elem in Network:
            if(elem.lower().find(line.lower())!=-1):
                elements.append(elem)
        elements.sort(key=len)
        for i in range(len(elements)):
            listelem+=elements[i]+", "
        printlines("",4)
        printlines(debug,1,"b")
        printlines("",2)
        printlines(item,1,"g")
        printlines(str(antworten),1)
        printlines(fragen[fragenummer],1)
        if(fragenummer==0):
            if(selected!=-1):
                printlines(listelem,1,elements[selected])
            else:
                printlines(listelem,1)
        else:
            if(antworten["parent"]!="-"):
                printlines(str(getco2valknown(str(antworten["parent"]))),1)
            else:
                printlines("",1)
        if(answer!=""):
            printlines(answer,1)
        else:
            printlines(line,1)
        sys.stdout.flush()
    Network[item.lower().strip()]=antworten

def zgmenu(zutaten,zutat,checkval=True):
    #printlines("",12)
    elements=[]
    possibleones=[]
    global lastzutat
    line=""
    selected=-1
    #deletelines(12)
    printlines("",3)
    printlines("last: "+lastzutat)
    printlines(debug,1,"b")
    printlines(", ".join(zutaten).replace('\n',' '),3,zutat)
    printlines(zutat,1,"g")
    printlines("Welcher Gruppe gehört diese Zutat an?",1)
    zutattmpsplit=zutat.split()
    if(len(zutattmpsplit)>1 and zutattmpsplit[0]==zutattmpsplit[1] and zutattmpsplit[0] in Network):
        Network[zutat]={"parent":zutattmpsplit[0]}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    zutattmp=zutat.replace("ae","ä").replace("ue","ü").replace("oe","ö")
    if(zutattmp in Network):
        Network[zutat]={"parent":zutattmp}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    zutattmp=zutattmp.replace("ss","ß").replace("ss","ß").replace("ss","ß")
    if(zutattmp in Network):
        Network[zutat]={"parent":zutattmp}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    zutattmp=zutat.replace("ss","ß").replace("ss","ß").replace("ss","ß")
    if(zutattmp in Network):
        Network[zutat]={"parent":zutattmp}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    if(zutat.startswith("ci")and RepresentsInt(zutat[2:])):
        Network[zutat]={"parent":"farbstoffe"}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    if(zutat.startswith("cl")and RepresentsInt(zutat[2:])):
        Network[zutat]={"parent":"farbstoffe"}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    zutattmp=zutat
    if(zutattmp.replace("kba","").strip().strip("*").strip() in Network):
        Network[zutat]={"parent":zutattmp.replace("kba","").strip().strip("*").strip()}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    zutattmp=zutat
    if(zutattmp.find("rebsorte")!=-1):
        Network[zutattmp.replace("rebsorte","").strip(":").strip()]={"parent":"rebsorte"}
        Network[zutat]={"parent":zutattmp.replace("rebsorte","").strip(":").strip()}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    if(zutat.startswith("e")and RepresentsInt(zutat[1:-1])):
        zutattmp=zutat[1:]
        if not RepresentsInt(zutattmp[-1]):
            zutattmp=zutattmp[:-1]
        if not RepresentsInt(zutattmp[-1]):
            zutattmp=zutattmp[:-1]
        if(RepresentsInt(zutattmp)):
            if int(zutattmp) in [120,441,542,901,904,910,913,920,921,966]:
                Network[zutat]={"parent":"-","Foodtype":3,"CO2_Eigene_Schaetzung":15000}
            elif int(zutattmp) in     [153,161,252,270,322,325,326,327,422,430,431,432,433,434,435,436,442,470,471,472,473,474,475,476,477,478,479,481,482,483,491,492,493,494,495,570,572,585,627,631,635,640]:
                Network[zutat]={"parent":"-","Foodtype":2,"CO2_Eigene_Schaetzung":15000}
            else:
                Network[zutat]={"parent":"-","Foodtype":0,"CO2_Eigene_Schaetzung":15000}
            if int(zutattmp)<200:
                Network[zutat]["parent"]="farbstoffe"
            elif int(zutattmp)<300:
                Network[zutat]["parent"]="konservierungsmittel"
            elif int(zutattmp)<500:
                Network[zutat]["parent"]="antioxidantien"
            elif int(zutattmp)<600:
                Network[zutat]["parent"]="rieselhilfen"
            elif int(zutattmp)<900:
                Network[zutat]["parent"]="geschmacksverstärker"
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
    critfactors=[". ",", ",": ","und ","kann ","aus "]
    for zutmaybe in Network:
        if(zutmaybe.replace(" . "," ").replace(". "," ").replace(", "," ").replace(": "," ").replace(" : "," ").replace("von "," ").replace("und "," ").replace("kann "," ").replace("aus "," ").replace("  "," ").replace("  "," ")==zutat):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        if(zutmaybe+"e"==zutat):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        if(zutmaybe+"n"==zutat):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        if(zutmaybe+"en"==zutat):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        if(zutmaybe[:-1]==zutat):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        if(zutmaybe.replace("-","")==zutat.replace("stuecke","").replace("-","")):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        zutattmp=zutat
        while(zutattmp[-1].isdigit() or zutattmp[-1]==" " or zutattmp[-1]=="." or zutattmp[-1]=="," or zutattmp[-1]==":" or zutattmp[-1]==";" or zutattmp[-1]==")" or zutattmp[-1]=="("):
            zutattmp=zutattmp[:-1]
            if(zutattmp==""):
                Network[zutat.lower().strip()]={"parent":"quatsch"}
                writeJSONFile()
                printlines("",2)
                return Network[zutat]
        while(zutattmp[0].isdigit() or zutattmp[-1]==" " or zutattmp[0]=="." or zutattmp[0]=="," or zutattmp[0]==":" or zutattmp[0]==";" or zutattmp[0]==")" or zutattmp[0]=="("):
            zutattmp=zutattmp[1:]
        if(zutmaybe==zutattmp or typocheck(zutat,zutmaybe)):
            Network[zutat]={"parent":zutmaybe}
            writeJSONFile()
            printlines("",2)
            return Network[zutat]
        if(len(zutmaybe)>2 and zutat.find(zutmaybe)!=-1):
            #print(zutmaybe)
            possibleones.append(zutmaybe)
    factorizedparent=findfactorizedparent(zutat)
    if(factorizedparent[0]!=""):
        Network[zutat]={"parent":factorizedparent[0],"factors":factorizedparent[1]}
        writeJSONFile()
        printlines("",2)
        return Network[zutat]
    if(zutat.find("konzentrat")!=-1):
        possibleones.append("mehrfruchtsaft fruchtkonzentraten")
        possibleones.append("fruchtkonzentrat")
        possibleones.append("gemüsekonzentrat")
    if(zutat.find("mark")!=-1):
        possibleones.append("obstmark")
    if(zutat.find("pulver")!=-1 or zutat.find("granul")!=-1 or zutat.find("extract")!=-1):
        possibleones.append("gemüsepulver")
        possibleones.append("fruchtkonzentrat")
        possibleones.append("gewürz")
    if(zutat.find("pueree")!=-1):
        possibleones.append("fruchtpueree")
    if(zutat.find("oel")!=-1 or zutat.find("oil")!=-1):
        possibleones.append("pflanzenoel")
    if(zutat.find("saft")!=-1):
        possibleones.append("obstsaft")
        possibleones.append("gemuesesaft")
    if(zutat.find("getrocknet")!=-1):
        possibleones.append("trockenobst")
        possibleones.append("trockengemuese")
    if(zutat.find("geroestet")!=-1):
        possibleones.append("geröstetes obst")
        possibleones.append("geröstetes gemüse")
    if(zutat.find("mehl")!=-1):
        possibleones.append("huelsenfruechtemehl")
        possibleones.append("getreidemehl")
    
    listelem=""
    possibleones.sort(key=len, reverse=True)
    if(zutat.find("fleisch")!=-1):
        possibleones.insert(0,"fleisch")
        possibleones.insert(0,"gefluegel")
        possibleones.insert(0,"rind")
    for i in range(len(possibleones)):
        listelem+=possibleones[i]+", "
    printlines(listelem.strip().strip(","),1)
    printlines("",1)
    #inputkey=readchar.readkey()
    #print(repr(inputkey))
    while True:
        inputchar=readchar.readkey()
        if(inputchar=='\x03'):
            quit()
        elif(inputchar=='\x1b\x1b'):
            return
        elif(inputchar=='#'):
            createzutatmenu(line)
            if(line!=zutat):
                Network[zutat.lower().strip()]={"parent":line.lower().strip()}
            break
        elif(inputchar==',' and line =="" and lastzutat!=""):
            del Network[lastzutat]
            zgmenu("",lastzutat)
        elif(inputchar=='.' and line ==""):
            Network[zutat.lower().strip()]={"parent":"quatsch"}
            lastzutat=zutat.lower().strip()
            break
        elif(inputchar=='-' and line ==""):
            Network[zutat.lower().strip()]={"parent":"-","Foodtype":2,"CO2_Eigene_Schaetzung":15000}
            lastzutat=zutat.lower().strip()
            break
        elif(inputchar=='\t'):
            if(line!=""):
                selected=(selected+1)%len(elements)
            else:
                selected=(selected+1)%len(possibleones)
        elif(inputchar=='\r'):
            if(selected!=-1 and line!="" and len(elements)>selected):
                Network[zutat]={"parent":elements[selected]}
                lastzutat=zutat.lower().strip()
                break
            elif(selected!=-1 and line=="" and len(possibleones)>selected):
                Network[zutat]={"parent":possibleones[selected]}
                lastzutat=zutat.lower().strip()
                break
            elif(len(elements)==1):
                Network[zutat]={"parent":elements[0]}
                lastzutat=zutat.lower().strip()
                break
            elif(line in elements):
                Network[zutat]={"parent":line.lower().strip()}
                lastzutat=zutat.lower().strip()
                break
            else:
                if(line!=""):
                    createzutatmenu(line)
                    if(line!=zutat):
                        Network[zutat.lower().strip()]={"parent":line.lower().strip()}
                        lastzutat=zutat.lower().strip()
                else:
                    createzutatmenu(zutat)
                break
        elif(inputchar=='\x7f'):
            if(len(line)>0):
                line=line[:-1]
                selected=-1
            elements=[]
            for elem in Network:
                if(elem.lower().find(line.lower())!=-1):
                    elements.append(elem)
        else:
            elements=[]
            selected=-1
            line+=inputchar
            for elem in Network:
                if(elem.lower().find(line.lower())!=-1):
                    elements.append(elem)
        deletelines(12)
        listelem=""
        elements.sort(key=len)
        if(line!=""):
            for i in range(len(elements)):
                listelem+=elements[i]+", "
        else:
            for i in range(len(possibleones)):
                listelem+=possibleones[i]+", "
        listelem=listelem.strip().strip(",")
        printlines("",4)
        printlines(debug,1,"b")
        printlines(", ".join(zutaten),3,zutat)
        printlines(zutat,1,"g")
        printlines("Welcher Gruppe gehört diese Zutat an?",1)
        if(selected!=-1):
            if(line!=""):
                printlines(listelem,1,elements[selected])
            else:
                printlines(listelem,1,possibleones[selected])
        else:
            printlines(listelem,1)
        printlines(line,1)
        #sys.stdout.write("\r\r\r\r"+line+"\ntest\n")
        sys.stdout.flush()
    writeJSONFile()
    return Network[zutat]
readJSONFile()
def printhelpmenu():
    print(* wrap("nutzung: python3 RelationsNet.py [optionen] zutatenliste/zutatendatei.txt"),sep="\n")
    print(* wrap("Das Script spuckt entsprechend der Zutatenliste zum aktuellen Zeitpunkt in Deutschland (achtung, saison und Ort relevant) CO2 äquivalente aus. diese Basieren auf Zahlen vom Öko-institut (https://www.oeko.de/oekodoc/328/2007-011-de.pdf), dem Pendos Verlag der das Öko-Institut und einige Weitere Zitiert, und eigener Schätzung basierend auf diesen Daten. Den Ursprung der Daten vom WWF kann ich nicht nachvollziehen, daher nutze ich die bisher nur im äussersten notfall."),sep="\n  ")
    print(* wrap("Die Daten sind nicht sonderlich genau, gerade dann nicht, wenn zutaten noch nicht bekannt oder eingeordnet sind, aber eine grobe Richtung sollten sie schon geben."),sep="\n  ")
    print("")
    print(* wrap("Die Zutatenliste sollte in anführungsstrichen angegeben werden, oder als pfad zu einer datei in der in jeder zeile eine zutatenliste angegeben ist"),sep="\n  ")
    print(* wrap("Die Zutatenlisten sollten wie gesetzlich eigentlich vorgesehen zutaten der Anteilgröße nach abwärts mit Prozentwerten (mehr prozentwerte -> mehr genau) angeben. des weiteren sollten diese Anteile mit Kommata sepperiert werden. bereits gemischte zutaten können in klammern angegeben werden."),sep="\n  ")
    print("")
    print("Beispiel:")
    print(* wrap("\tpython3 RelationsNet.py \"Maisstärke, Kakaopulver 22%, Meersalz 1%\""),sep="\n\t")
    print("")
    print("Optionen:")
    print("\t --isbio")
    print(* wrap("\t \t Markiert das Produkt als produkt aus Biologischem Anbau, nach Pendos CO2-Zähler hat der Bio anbau einen einfluss von 10-25 Prozent auf ein frisches Produkt, die genaue quelle die dieser Zähler Zitiert habe ich bisher nicht gefunden, wenn ihr was findet, meldet es mir bitte, ich bin nur ein übernächtigter student, ich mache ohne ende Fehler"),sep="\n\t\t  ")
    print("\t --region:[region]")
    print(* wrap("\t \t setzt die Herkunftsregion oder Transportwege des Produkts, hier gibt es \"regional\" mit 230g (CO2äq/kg Lebensmittel),\"europa\" mit 460g,\"schiff\" mit 570g"),sep="\n\t\t  ")
    print("\t --packaging:[packaging]")
    print(* wrap("\t \t setzt die Verpackungsart, hier gibt es bisher der einfachheit halber nur \"unverpackt\" mit einem Wert von 30g CO2 pro kilo Lebensmittel da auch diese Lebensmittel für den Transport verpackt werden, pfand mit einem wert von 150 und einen Default wert von 300"),sep="\n\t\t  ")
    print("\t --v")
    print(* wrap("\t \t Spuckt jede Menge weitere Informationen aus, woher werte kommen, und debug informationen, eignet sich eher für Menschen die auch den Code lesen"),sep="\n\t\t  ")
    print(* wrap("\t --activate_Interface"),sep="\n\t")
    print(* wrap("\t \t Aktiviert ein interface das im falle von neuen (nicht eingeordneten) Zutaten erfragt, zu welchen Gruppen es gehört."),sep="\n\t\t")
    print(* wrap("\t \t Das Interface kann jederzeit mit strg+c beendet werden."),sep="\n\t\t")
    print("\t \t ")
    print(* wrap("\t \t wenn das Interface einen Tippfehler vermutet, ordnet es die Zutat automatisch ein, dies kann fehlerhaft sein, Pils ist kein Pilz beispielsweise, daher werden diese outputs als ganze blöcke noch angezeigt, kommt ein ganzer block hinzu sollte überprüft werden, was zugeordnet wurde, und im falle des falles am ende der Networkfile.json korrigiert werden"),sep="\n\t\t  ")
    print("\t \t ")
    print(* wrap("\t \t ansonsten werden mögliche Gruppen angezeigt, die können mit Tab ausgewählt werden (Das highlight markiert leider das erste vorkommen der genauen zeichenfolge, bei Schweinefleisch, fleisch kann also das fleisch von schweinefleisch markiert sein, gemeint ist aber fleisch) akzeptiert wird mit enter."),sep="\n\t\t  ")
    print(* wrap("\t \t wenn die Gruppe nicht angezeigt wird, kann drauf los getippt werden, dann werden anhand des getippten möglichkeiten angezeigt, die können ebenso mit tab markiert und akzeptiert werden."),sep="\n\t\t  ")
    print(* wrap("\t \t ist eine Gruppe nicht genau so vorhanden ist wie man möchte, kann mit # genau der Name einer gruppe akzeptiert werden, den man eingegeben hat. Dann wird die Gruppe erstellt und anhand von Fragen durch die Erstellung dieser Gruppe geleitet, ein möglicher parent (obergruppe) wird angezeigt um abweichungen davon abzulesen."),sep="\n\t\t  ")
    print(* wrap("\t \t wenn eine zutat syntetisch ist, kann es sein das man dem ganzen einen Standartwert zuweisen möchte, hierfür ist die taste \"-\" ohne eingabe von text im hauptmenü. Dann wird der CO2 Äquivalente wert auf 15000 gesetzt und keinem Parent zugeordnet. (vielleicht sollte hier ein parent stehen, um die auffindbar zu halten)"),sep="\n\t\t  ")
    print(* wrap("\t \t wenn ein Text definitiv keine Zutateninformation ist, wie \"aus biologischem anbau\" kann man das ganze im script einerseits in die factorstexts (komplett kleingeschrieben, ohne umlaute) eintragen damit entsprechende flags gesetzt werden dass dieses Produkt warscheinlich komplett bio ist, bei fangmethoden von fischen, oder farben, so werten die keine weitere zutat mit eigenen CO2 Äquivalent beeinflussenden eigenschaften haben, wird mit \".\" im hauptmenü ohne eingabe von text das ganze \"quatsch\" zugeordnet, und beim berechnen des CO2 Äquivalente wertes übersprungen"),sep="\n\t\t  ")
    print(* wrap("\t \t hat man bei der Letzten zutat einen Fehler gemacht, so kann man mit \",\" ohne eingabe von text zu diesem wert zurückspringen und den Fehler korrigieren"),sep="\n\t\t  ")


if len(sys.argv)>1:
    biobool=False
    region="schiff"
    packaging=""
    verbosebool=False
    skipnewbool=True
    for i in range(1,len(sys.argv)):
        if(sys.argv[i].startswith("--isbio")):
            biobool=True
        if(sys.argv[i].startswith("--v")):
            verbosebool=True
        if(sys.argv[i].startswith("--h")):
            printhelpmenu()
        if(sys.argv[i].startswith("--activate_interface")):
            skipnewbool=False
        if(sys.argv[i].startswith("--region:")):
            region=sys.argv[i][9:]
        if(sys.argv[i].startswith("--packaging:")):
            packaging=sys.argv[i][12:]
        else:
            if(sys.argv[i].endswith(".txt")):
                fileliste=open(sys.argv[i])
                for lineliste in fileliste:
                    printlines(str(getCO2(lineliste,skip_new=skipnewbool,actpackaging=packaging, actregion=region,isbio=biobool,verbose=verbosebool))+" g CO2(Äquivalente) für "+lineliste.replace("\n",""),1,"g")
            else:
                printlines(str(getCO2(sys.argv[i],skip_new=skipnewbool,actpackaging=packaging, actregion=region,isbio=biobool,verbose=verbosebool))+" g CO2(Äquivalente) für "+sys.argv[i].replace("\n",""),1,"g")
else:
    printhelpmenu()


###Example code for reading From File
#else:
#    fileliste=open("Unverpackt_Siegen/liste.txt")
#    print("#####################################################################")
#    for lineliste in fileliste:
        #printlines(#,actpackaging="",actregion="",isbio=True
#        printlines(str(getCO2(lineliste,skip_new=False,actpackaging="unverpackt", actregion="regional",isbio=True)),1,"g")
    #getCO2(lineliste,skip_new=False,actpackaging="unverpackt",actregion="regional",isbio=True)
#amountitems=float(len(zutatenraw))
#itemposition=float(0)


###Example code for DB
#cm = sqlite3.connect("Waren_mined.db")
#curm = cm.cursor()
#curm.execute("SELECT zutatenraw from Directinfo where not zutatenraw='' ORDER BY LENGTH(zutatenraw);")
#zutatenraw=curm.fetchall()
#for zutatenliste in zutatenraw:
#    itemposition+=1.0
#    if(zutatenliste[0]!=""):
#        debug="at "+str(int(itemposition))+" of "+str(int(amountitems))+": "+str((itemposition/amountitems)*100)+"%"
        #getCO2(zutatenliste[0],skip_new=True)
#        print(str(getCO2(zutatenliste[0],skip_new=True)))

###example code for getting the most important skipped ones
#for k in sorted(ToDealWith, key=ToDealWith.get, reverse=True):
#    zgmenu([k+" "+str(ToDealWithTexts[k])+" "+str(ToDealWith[k])],k)
    #print(k,ToDealWith[k])
# Erst nach Parrent fragen, wenn parrent schon Fleisch oder Vegan ist, dann ist Child entsprechend
